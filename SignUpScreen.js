// SignUpScreen
import React, { useState } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity } from 'react-native';

export default function SignUpScreen({ navigation }) {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [phone, setPhone] = useState('');
    const [residence, setResidence] = useState('');
    const [floor, setFloor] = useState('')

    const handleNext = () => {
        navigation.navigate('Verification');
    };

    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={() => navigation.goBack()} style={styles.backButton}>
                <Text style={styles.backButtonText}>←</Text>
            </TouchableOpacity>

            <Text style={styles.heading}>What's your name?</Text>
            <View style={styles.row}>
                <TextInput style={[styles.input, styles.inputLeft]} placeholder="First" placeholderTextColor="grey" />
                <TextInput style={[styles.input, styles.inputRight]} placeholder="Last" placeholderTextColor="grey" />
            </View>

            <Text style={styles.heading}>What is your email?</Text>
            <TextInput style={styles.input} placeholder="Columbia email" keyboardType="email-address" placeholderTextColor="grey" />

            <Text style={styles.heading}>What is your phone number?</Text>
            <TextInput style={styles.input} placeholder="XXX-XXX-XXXX" keyboardType="phone-pad" placeholderTextColor="grey" />

            <Text style={styles.heading}>Where do you live?</Text>
            <TextInput style={styles.input} placeholder="Residence Hall" placeholderTextColor="grey"/>

            <Text style={styles.heading}>What floor?</Text>
            <View style={styles.row}>
                <TextInput style={[styles.input, styles.inputSmall]} placeholder="10" keyboardType="number-pad" placeholderTextColor="grey"/>
            </View>

            <TouchableOpacity style={styles.button} onPress={handleNext} >
                <Text style={styles.buttonText}>Next</Text>
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#161616',
        padding: 30,
    },
    backButton: {
        marginTop: 48,
    },
    backButtonText: {
        color: '#FFF',
        fontSize: 24,
    },
    heading: {
        color: '#fff',
        fontSize: 24,
        marginTop: 24,
        marginBottom: 24,
    },
    input: {
        color: '#fff',
        borderBottomColor: '#fff',
        borderBottomWidth: 1,
        fontSize: 20,
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    inputLeft: {
        flex: 1,
        marginRight: 48,
    },
    inputRight: {
        flex: 1,
    },
    inputSmall: {
        flex: 1,
        marginRight: 300,
    },
    button: {
        backgroundColor: '#000000',
        paddingVertical: 20,
        paddingHorizontal: 100,
        borderRadius: 10,
        marginTop: 68,
        marginBottom: 80,
    },
    buttonText: {
        color: '#fff',
        textAlign: 'center',
        fontSize: 20,
    },
});